#!/bin/bash

if [[ -z $DESTINATION_URL ]]; then
   echo "No DESTINATION_URL variable was defined"
   exit 0;
fi

if [[ -z $TIME_INTERVAL ]]; then 
    echo "No TIME_INTERVAL variable is set" 
    exit 1
fi

while true; do
    curl -w "@/opt/app-root/loop_curl_statistics.txt" -o /dev/null -s "$DESTINATION_URL"
    sleep $TIME_INTERVAL
done
