#!/bin/bash

if [[ -z $DESTINATION_URL ]]; then
   echo "No DESTINATION_URL variable was defined"
   exit 1;
fi

/usr/sbin/httpd $OPTIONS -DFOREGROUND
